1. Psycho-Pass (5)
2. Black Lagoon
   1. Black Lagoon (5)
   2. Black Lagoon: Roberta's Blood Trail
3. Re: Zero kara Hajimeru Isekai Seikatsu (5)
4. Suisei no Gargantia (3)
    1. Suisei no Gargantia: Meguru Kouro, Haruka (2)
5. Death March kara Hajimaru Isekai Kyousoukyoku (3)
6. Gangsta (3)
7. Gate: Jieitai Kanochi nite, Kaku Tatakaeri (2)
8. Hamatora the Animation (3)
    1. Re: Hamatora (3)
9.  Kiseijuu: Sei no Kakuritsu (5)
10. Trickster: Edogawa Ranpo "Shounen Tanteidan" yori (2)
11. Quanzhi Fashi (-)
12. Tensei shitara Slime Datta Ken
13. To aru majutsu no indekkusu (-)
14. Kokkoku (5)
15. Cowboy Bebop (4)
    1. Cowboy Bebop: The Movie (4)
16. Steins;Gate (4)
    1. Gekijouban Steins;Gate: Fuka Ryouiki no Deja vu
    2. Steins;Gate: Soumei Eichi no Cognitive Computing
    3. Steins;Gate 0
17. Chaos; Head (4)
    1. Chaos;Child
    2. ChaoS;Child: Silent Sky
18. Dorohedoro (5)
19. Darwin's Game (3)
20. Hachi-nan tte, Sore wa Nai deshou! The 8th son? Are you kidding me? (2)
21. Shinchou Yuusha: Kono Yuusha ga Ore Tueee Kuse ni Shinchou Sugiru (2)
22. Gleipnir (5)
23. Maou Gakuin no Futekigousha (2)
24. Mushoku Tensei: Isekai Ittara Honki Dasu (4)
25. Jujutsu Kaisen (3)
26. Deatte 5-byou de Battle (2)
27. Vivy: Fluorite Eye's Song (2)
28. Fumetsu no Anata e (2)
29. Meikyuu Black Company (5)
30. Tsuki ga Michibiku Isekai Douchuu (3)
31. Saihate no Paladin (2)
32. Platinum End (3)
33. Heion Sedai no Idaten-tachi (3)
34. Isekai Yakkyoku (3) - in progress
35. Majo no Tabitabi (2) - in progress
36. Yuukoku no Moriarty (3)
37. Kage no Jitsuryokusha ni Naritakute! (5)
38. Chainsaw Man (2)
39. Dungeon ni Deai wo Motomeru no wa Machigatteiru Darou ka (?)
40. JoJo no Kimyou na Bouken
    1.  JoJo no Kimyou na Bouken Part 6: Stone Ocean (3)
41. Dead Mount Death Play (4)
42. Mashle (2)
43. Kaminaki Sekai no Kamisama Katsudou (3)
44. Kimetsu no Yaiba (4)
45. Isekai Shoukan wa Nidome desu (2)
46. I Got a Cheat Skill in Another World and Became Unrivaled in The Real World, Too (2)
47. Edomae Elf (2)