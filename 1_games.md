- [Список игр](#список-игр)
- [Как правильно установить и настроить некоторые игры](#как-правильно-установить-и-настроить-некоторые-игры)
  - [Какие видеокарты покупать и почему](#какие-видеокарты-покупать-и-почему)
  - [Как правильно установить Skyrim Special Edition](#как-правильно-установить-skyrim-special-edition)

# Список игр

* By Lovecraft
  * Call of Cthulhu
  * The Sinking City
* Dishonored
  * Dishonored
  * Dishonored 2
* BioShock
  * BioShock Infinit
  * BioShock Infinit: Burial at Sea - have not finished yet
* Metro
  * Metro 2033
  * Metro: Last Light
  * Metro Redux
  * Metro Exodus
* Heroes of Might and Magic
  * Heroes of Might and Magic III
  * Heroes of Might and Magic IV
  * Heroes of Might and Magic V
  * Heroes of Might & Magic Heroes VI
  * Heroes of Might & Magic Heroes VII - didn't try
* Far Cry
  * Far Cry 3
  * Far Cry 4
  * Far Cry 5
* The Witcher
  * The Witcher 3
* Wolfenstein
  * Wolfenstein: The New Order
* The Elder Scrolls
  * The Elder Scrolls III: Morrowind
  * The Elder Scrolls IV: Oblivion
  * The Elder Scrolls V: Skyrim
* Fallout
  * Fallout 3 - didn't try
    * Fallout: New Vegas - didn't try
* Disciples
  * Disciples II
    * Disciples II: Dark Prophecy
    * Disciples II: Gallean’s Return
    * Disciples II: Rise of the Elves
  * Disciples III: Renaissance - didn't try
    * Disciples III: Resurrection
    * Disciples III: Reincarnation
* Warcraft
  * Warcraft III: Reign of Chaos
  * Warcraft III: The Frozen Throne
* Painkiller
* Half-Life
  * Half-Life
  * Half-Life 2
    * Half-Life 2: Lost Coast
    * Half-Life 2: Episode One
    * Half-Life 2: Episode Two
  * Portal - spin off of Half-Life
    * Portal
    * Portal 2
* Warhammer 40,000 - boring, can try to check design only
  * Warhammer 40,000: Dawn of War
  * Warhammer 40,000: Dawn of War — Dark Crusade
  * Warhammer 40,000: Dawn of War – Soulstorm
* Doom
  * Doom 3
  * Doom 2016 - cool and beautiful
  * Doom Eternal - didn't finish, not so beautiful visually

# Как правильно установить и настроить некоторые игры 
## Какие видеокарты покупать и почему
пусто

## Как правильно установить Skyrim Special Edition
Эта инструкция только для английской версии, потому что мод **Unofficial Skyrim Special Edition Patch** работает только с ней.

- **Что понадобится:**
  1. Установить **Skyrim Special Edition** (SE) - версия 1.5.97.0.8 и выше
  2. Скачать программа загрузчик через которую запускать Skyrim для расширения возможностей скриптов (нужна для работы SkyUI) **Skyrim Script Extender** (SKSE) http://skse.silverlock.org/
  3. Скачать удобная менюшка игры для компа **SkyUI SE** https://www.nexusmods.com/skyrimspecialedition/mods/12604?tab=files
  4. Скачать неофициальный патч багов **Unofficial Skyrim Special Edition Patch** https://www.nexusmods.com/skyrimspecialedition/mods/266
  5. **LOOT** - программа для сортировки модов (расставления в правильном порядке), обновленный список порядка загрузки модов умеет грузить с интернета (не умеет из активировать) https://loot.github.io/
  6. **Nexus Mod Manager** - сторонний менеджер модов через который можно активировать моды https://www.nexusmods.com/about/vortex/
- **Как установить**
  1. Распокавать skse в каталог установленного Skyrim как есть (кроме каталога src, он не нужен)
  2. Распаковать SkyUI и Unofficial Skyrim Special Edition Patch в каталог Data внутри установленного Skyrim
  3. Отредактировать файл c:\Users\user\AppData\Local\Skyrim Special Edition\plugins.txt так (звездочка означает включен):
      ```
      *Unofficial Skyrim Special Edition Patch.esp
      *SkyUI.esp
      ```
  4. Запустить и установить программу LOOT, нажать в ней кнопку СОРТИРОВАТЬ чтобы установить моды в правильном порядке. Это на всякий случай, в файле plugins.txt они и так должны быть в правильном.
  5. Skyrim запускать только через файл skse64_loader.exe (из skse) и не пользоваться родными SkyrimSE.exe или SkyrimSELauncher.exe. Иначе может слететь активация SkyUI, потому что плохой встроенный менеджер модов может его отключить. Посмотреть активацию модов можно в программе LOOT, если стоит зеленая галочка стоит значит мод активен. Если активация SkyUI слетела, то выполнить пунк 3 (закрыть Skyrim, отредактировать файл, запустить заново)
  6. Если Skyrim не грузится после активации SkyUI, то установить Nexus Mod Manager (NMM) и активировать SkyUI через него

**Note.**  
1 Чтобы установить skse нужно скачать архив с сайта и распаковать в каталог Skyrim (там где exe и dll). После этого запускать игру через запуск skse64_loader.exe от Администратора (правок кнопкой мыши)  
2 Чтобы всегда запускать от Администратора: в свойствах ярлыка раздел Совместимость поставить галочку "запускать от администратора"

Чтобы активировать **SkyUI** можно вручную добавить его в файл: `c:\Users\user\AppData\Local\Skyrim Special Edition\plugins.txt`  
Содержимое `plugins.txt`:
```
*Skyrim.esm
*Update.esm
**Dawnguard.esm
HearthFire.esm
*Dragonborn.esm
*Unofficial Skyrim Special Edition Patch.esp
*SkyUI.esp
[The rest of your mods]
```
**Note.** файлы Skyrim.esm, Update.esm, Dawnguard.esm, HearthFire.esm, *Dragonborn.esm - это стандартные файлы оригинального **Skyrim Special Edition**  
**Note.** в списке модов можно заметить файл Update.esm он относится к обновлениям и **LOOT** расставляет его правильно.