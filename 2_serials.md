- [TV series by comics](#tv-series-by-comics)
- [TV series by books](#tv-series-by-books)
- [TV series by anime](#tv-series-by-anime)
- [TV series by games](#tv-series-by-games)
- [Про праздники](#про-праздники)
- [Фэнтези (5)](#фэнтези-5)
- [Фэнтези (4)](#фэнтези-4)
- [Фэнтези (3)](#фэнтези-3)
- [Фэнтези (-)](#фэнтези-)
- [Фантастика (5)](#фантастика-5)
- [Фантастика (4)](#фантастика-4)
- [Фантастика (3)](#фантастика-3)
- [Фантастика (-)](#фантастика-)
- [Не фантастика (5)](#не-фантастика-5)
- [Не фантастика (4)](#не-фантастика-4)
- [Не фантастика (3)](#не-фантастика-3)
- [Не фантастика (2)](#не-фантастика-2)
- [Не фантастика (-)](#не-фантастика-)
- [Комедии (5)](#комедии-5)
- [Комедии (4)](#комедии-4)
- [Комедии (3)](#комедии-3)
- [Комедии (-)](#комедии-)
- [Драмы и мелодрамы (5)](#драмы-и-мелодрамы-5)
- [Ужасы и мрачная мистика](#ужасы-и-мрачная-мистика)
- [Queue to watch](#queue-to-watch)

# TV series by comics
1. Marvel
    1. Daredevil (5)
    2. Jessica Jones 5)
    3. Luke Cage (5)
    4. The Punisher (5)
    5. Iron Fist (5)
2. DC
    1. Gotham (5)
    2. Titans (2)
    3. Doom Patrol (5)
    4. Naomi (2)
3. Marvel
    1. Legion (5)
    2. The Gifted (3)
4. Marvel
    1. Cloak & Dagger (3)
5. Marvel
    1. Runaways (3)
6. Marvel
   1. WandaVision (3)
   2. The Falcon and the Winter Soldier (3)

# TV series by books
1. Stephen King
    1. Mr. Mercedes (5)
    2. Castle Rock
    3. Haven
    4. Under the Dome
2. The Vampire Diaries (2)
    1. The Originals (5)
    2. Legacies (3)
3. Neil Gaiman
    1. American Gods (5)
    2. Good Omens (4)
    3. The Sandman, 2022 (5) - also by DC Comics
4. George R. R. Martin
   1. Nightflyers (2)
   2. Game of Thrones
      1. Game of Thrones (5)
      2. House of the Dragon (4)
5. Matthew Ruff
   1. Lovecraft Country (3) - it has motive of Lovecraft stories (it is not an adoptation of Lovecraft). But the show is not very interesting, but it has high quality
6. Terry Pratchett
   1. The Watch
7. Star Wars
    1. The Mandalorian (3)
    2. The Book of Boba Fett (3)
    3. Ahsoka (3)
8. J. R. R. Tolkien
   1. The Lord of the Rings: The Rings of Power, 2022 (4)
9. Isaac Asimov
   1.  Foundation (5)

# TV series by anime
1. Cowboy Bebop, 2021 (4)
2. One Piece (5)

# TV series by games
1. Castlevania
   1. Castlevania
   2. Castlevania: Nocturne (5)

# Про праздники
* Christmas
  * A Christmas Carol, 2019

# Фэнтези (5)
1. Into the Badlands
2. Stranger Things
3. Salem
4. The Twilight Zone
   1. The Twilight Zone (5)
   2. The Twilight Zone, 2019 (-)
5. The Exorcist
6. Heroes
7.  Heroes Reborn
8.  The Magicians
9.  Dirk Gently's Holistic Detective Agency
10. Iron Fist
11. Jonathan Strange & Mr Norrell
12. Preacher
13. Russian Doll
14. Dracula, 2020
15. The Boys
16. Chilling Adventures of Sabrina
17. Undone
18. His Dark Materials
19. Arcane, 2021
20. Invincible, 2020
21. Midnight Mass, 2021
22. One Piece

# Фэнтези (4)
1. Kindred: The Embraced
2. Aftermath
3. Johnny and the Bomb
4. The Outer Limits
5. Powers
6. The Frankenstein Chronicles
7.  Siren
8.  Carnival Row
9.  Penny Dreadful: City of Angels, 2020
10. Penny Dreadful, 2014 - некоторые серии шедевр, некоторые невозможно скучны
11. Castle Rock, 2018 ???
12. Warrior Nun - сюжет слабый, но увлекательный и спец. эффекты норм, актеры неплохие
13. Moon Knight - too simple but it has good actors and special effects
14. Motherland: Fort Salem, 2020 - not very good special effects
15. Shadow and Bone, 2021
16. The Addams Family
    1.  Wednesday, 2022 (4)

# Фэнтези (3)
1. The Hunger
2. Neverwhere
3. The Nightmare Worlds of H.G. Wells
4. Second Chance
5. Constantine
6. Supernatural
7. Doctor Who
   1. Doctor Who (3)
   2. Class (2)
8. Grimm
9.  Lost
10. From Dusk Till Dawn
11. iZombie
12. Jekyll & Hyde
13. Camelot
14. Once Upon a Time
15. The Shannara Chronicles
16. The Umbrella Academy
17. The Order ???
18. October Faction ???
19. The Dead Lands ???
20. Emerald City
21. Raising Dion
22. Reign ???
23. Locke & Key
24. Strowlers
25. Cursed, 2020
26. Juda, 2017 - Israel, about vampire, comedy
27. The Nevers, 2021
28. Daybreak, 2019 - not very good plot

# Фэнтези (-)
1. Teen Wolf
2. Hemlock Grove
3. Shadowhunters
4. The Wizards of Aus
5. Sense8
6. Lucifer
7. Continuum: Web Series
8. A Discovery of Witches
9. Charmed [2018],(2 ?)
10. Damien
11. Tell Me a Story
12. Wynonna Earp
13. Charlie Jade
14. Fate: The Winx Saga, 2021

# Фантастика (5)
1. Black Mirror
2. Farscape
3. Travelers
4. Z Nation
5. Extant
6. The Man in the High Castle
7. Mr. Robot
9. Limitless
10. The X Files
11. Westworld
12. The Expanse
13. Altered Carbon
15. Impulse
16. Minority Report
17. Origin
18. The Peripheral, 2022

# Фантастика (4)
1. The Walking Dead
   1. The Walking Dead (3)
   2. The Walking Dead: Oath
   3. Fear the Walking Dead (3)
2. 12 Monkeys
3. The Strain
4. Firefly
5. Lost in Space
6. Love, Death & Robots
7. See, 2019 - in progress
8. The Society, 2019
9. The Wheel of Time, 2021

# Фантастика (3)
1. Continuum
2. Dark Matter
3. Humans
4. Jericho
5. The Last Ship
6. Childhoods End
7. Dark Angel
8. Frequency
9. The Mist
10. 11.22.63
11. Time After Time
12. Salvation
13. V-Wars
14. Timeless, 2016
15. Outlander, 2019
16. Raised by Wolves - a lot of logic mistakes but it has good actors and average effects
17. The Man Who Fell to Earth - too simple
18. Halo - too simple
19. Resident Evil, 2022 - in progress

# Фантастика (-)
1. Scorpion
2. The Last Man on Earth
3. Wisdom of the Crowd
4. The Rain
5. The First
6. Reverie
7. Kiss Me First
8. The Aliens, 2016
9.  Project Blue Book
10. Away, 2020 - stupid plot

# Не фантастика (5)
1. Fargo
2. The Americans
3. True Detective
4. Deutschland 83
6. HOUSE M.D
7. Breaking Bad
   1. Breaking Bad (4)
   2. Better Call Saul (4)
8. Black Sails
9.  Borgia
10. The Night Of
11. Dickensian
12. Dirk Gently
13. Bates Motel
14. The Alienist
15. You ???
16. Jack Reacher
17. The Flight Attendant, 2021
18. Vikings, 2013
19. Warrior, 2020

# Не фантастика (4)
1. Tom Clancy's Jack Ryan
2. Halt and Catch Fire
3. The End of the F***ing World
4. Chernobyl
5. Endeavour
6. Outer Banks, 2020

# Не фантастика (3)
1. Deadly Class
2. Bodyguard, 2018
3. Hanna, 2019
4. Warrior, 2019 ???
5. I Am the Night ???
6. Mindhunter - dark and heavy detective, but it has good quality

# Не фантастика (2)
5. MacGyver
2. Strange Angel

# Не фантастика (-)
1. Sons of Anarchy
2. Marco Polo

# Комедии (5)
1. Silicon Valley
2. Frasier
3. Surviving Jack
4. The Middle
6. The Guild
7. Suburgatory
8. Scrubs
9. Wilfred
10. A Young Doctor's Notebook
11. Detectorists
12. House of Lies
13. Backstrom
14. The Big Bang Theory
    1. Young Sheldon
15. Cobra Kai
16. Black Monday
17. Ghosts, 2021
18. Young Rock, 2021 - in progress
19. Rick and Morty
    1.  Rick and Morty
    2.  The Vindicators
20. Sex Education

# Комедии (4)
1. Betas
2. Zapped
3. London Irish
4. The Odd Couple
5. Stan Against Evil
6. The Young Offenders
7. Ghosts, 2019
8. The Rookie (s: 1, 2)
9. FUBAR, 2023

# Комедии (3)
1. Black Jesus
2. Bewitched
3. Tripped
4. Weeds

# Комедии (-)
1. Into the Dark
2. People Of Earth

# Драмы и мелодрамы (5)
1. Euphoria (s: 1) - some of episods are boring, some of them are interesting but entire show has high quality
2. Dare Me, 2019 (s: 1) - light and dynamic
3. The Queen's Gambit (s: 1) - interesting but better you don't watch it more then once

# Ужасы и мрачная мистика
1. The Haunting of Hill House

# Queue to watch
1. Amazing Stories, 2020
2. The Dark Crystal: Age of Resistance - boring I suppose
3. Making History, 2017
4. Anamnesis, 2015
5. Believe, 2014
6. The Whispers, 2015
7. Pretender, 1996
8. Lightfields, 2013
9. Uncle, 2014
10. Impastor, 2015
11. South of Hell, 2015
12. Houdini and Doyle, 2016
13. Now Apocalypse, 2019
14. Black Summer, 2019
15. NOS4A2, 2019 - written by Joe Hill that is a Stephen King son
16. The InBetween, 2019
17. Are You Afraid of the Dark, 2019
18. For All Mankind, 2019 - too boring and not logical plot
19. The Innocents, 2018 - low rating
20. Heartless, 2014
21. The Returned, 2015
22. Resurrection, 2014
23. Stitchers, 2015
24. Wolf Lake, 2001
25. The Leftovers, 2014 - may need to see
26. The Messengers, 2015
27. Seven Days, 1998
28. Mike & Molly, 2010
29. Rake, 2013 - low rating
30. Monsterland, 2020 - low rating
31. Barbarians, 2020 - may need to see